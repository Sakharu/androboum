package com.example.sakharu.androboum;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.UnsupportedEncodingException;

public class NearbyService extends Service
{
    final String SERVICE_ID = "123";

    public NearbyService() {

    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        //on ne se connecte plus à l'API Nearby via le google api client car cette façon de faire est obsolète
        //on se va se connecter directement à l'API Nearby dans les méthodes startDiscovery et startAdvertising

        //et on démarre la découverte des appareils à proximité
        startDiscovery();
        startAdvertising();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final EndpointDiscoveryCallback mEndpointDiscoveryCallback =
            new EndpointDiscoveryCallback() {
                @Override
                public void onEndpointFound(@NonNull String endpointId, @NonNull DiscoveredEndpointInfo discoveredEndpointInfo)
                {
                    Nearby.getConnectionsClient(getApplicationContext()).requestConnection(
                            MapsActivity.monmail,
                            endpointId,
                            mConnectionLifecycleCallback)
                            .addOnSuccessListener(
                                    new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void unusedResult) {
                                            // On a envoyé une requête de connexion
                                        }
                                    })
                            .addOnFailureListener(
                                    new OnFailureListener()
                                    {
                                        @Override
                                        public void onFailure(@NonNull Exception e) { }
                                    });
                }

                @Override
                public void onEndpointLost(@NonNull String endpointId)
                {
                    // Quelqu'un vient de partir
                }
            };

    private void startDiscovery() {
        Nearby.getConnectionsClient(getApplicationContext()).startDiscovery(
                SERVICE_ID,
                mEndpointDiscoveryCallback,
                new DiscoveryOptions(Strategy.P2P_POINT_TO_POINT))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>()
                        {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                //On démarre la découverte des appareils à proximité
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener()
                        {
                            @Override
                            public void onFailure(@NonNull Exception e)
                            {
                                //Impossible de démarrer la découverte des appareils à proximité
                            }
                        });
    }

    private final ConnectionLifecycleCallback mConnectionLifecycleCallback = new ConnectionLifecycleCallback()
            {
                @Override
                public void onConnectionInitiated(@NonNull String endpointId, @NonNull ConnectionInfo connectionInfo)
                {
                    // On accepte les connexions entrantes
                    Nearby.getConnectionsClient(getApplicationContext()).acceptConnection(endpointId, payloadCallback);
                }

                @Override
                public void onConnectionResult(@NonNull String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode())
                    {
                        //La connexion a réussi, on peut envoyer des données
                        case ConnectionsStatusCodes.STATUS_OK:
                            try
                            {
                                byte[] bytes = MapsActivity.monmail.getBytes("UTF-8");
                                Payload bytesPayload = Payload.fromBytes(bytes);
                                Nearby.getConnectionsClient(getApplicationContext()).sendPayload(endpointId, bytesPayload);
                            }
                            catch (UnsupportedEncodingException e) {e.printStackTrace(); }
                            break;

                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            // La connexion a été rejeté par au moins une des deux parties
                            break;

                        case ConnectionsStatusCodes.STATUS_ERROR:
                            //Erreur de connexion
                            break;
                    }
                }

                @Override
                public void onDisconnected(@NonNull String endpointId) { }
            };

    private void startAdvertising() {
        AdvertisingOptions advertisingOptions = new AdvertisingOptions.Builder().setStrategy(Strategy.P2P_POINT_TO_POINT).build();
        Nearby.getConnectionsClient(getApplicationContext())
                .startAdvertising(MapsActivity.monmail, SERVICE_ID, mConnectionLifecycleCallback, advertisingOptions)
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                // On se met en détectable par les autres
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Erreur de mise en détection
                            }
                        });
    }

    private PayloadCallback payloadCallback = new PayloadCallback() {
        @Override
        public void onPayloadReceived(@NonNull String s, Payload payload)
        {
            byte[] b = payload.asBytes();
            String content = null;
            if (b != null)
            {
                content = new String(b);
            }
            System.out.println(content);


            final NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"default")
                    .setSmallIcon(R.mipmap.icon_bomb)
                    .setContentTitle("Androboum")
                    .setContentText("Nouvel utilisateur à proximité : "+content)
                    .setVibrate(new long[]{0,1000})
                    .setAutoCancel(true);

            final NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                builder.setChannelId("10");
                NotificationChannel channel = new NotificationChannel("10", "Androboum", NotificationManager.IMPORTANCE_DEFAULT);
                if (notificationManager != null)
                    notificationManager.createNotificationChannel(channel);
            }
            if (notificationManager != null)
                notificationManager.notify(10,builder.build());
        }

        @Override
        public void onPayloadTransferUpdate(@NonNull String s, @NonNull PayloadTransferUpdate payloadTransferUpdate) { }
    };
}

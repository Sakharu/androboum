package com.example.sakharu.androboum;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

class MyPagerAdapter extends PagerAdapter {
    List<Profil> liste;
    Context context;
    MyPagerAdapter(Context context, List<Profil> liste) {
        this.liste = liste;
        this.context = context;
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        // on va chercher la layout
        ViewGroup layout = (ViewGroup) View.inflate(context, R.layout.activity_other_user, null);
        // on l'ajoute à la vue
        container.addView(layout);
        // on le remplit en fonction du profil
        remplirLayout(layout, liste.get(position));
        // et on retourne ce layout
        return layout;
    }
    @Override
    public int getCount() {
        return liste.size();
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
    private void remplirLayout(ViewGroup layout, final Profil p) {
        ImageView imageView = layout.findViewById(R.id.iprofilother);
        ImageView imageView2 = layout.findViewById(R.id.connecteother);
        TextView textView = layout.findViewById(R.id.emailuser);
        TextView textScore = layout.findViewById(R.id.scoreOtherUser);

        // on télécharge dans le premier composant l'image du profil
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference photoRef = storage.getReference().child(p.getEmail() + "/photo.jpg");
        GlideApp.with(context)
                .load(photoRef)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.ic_person_black_24dp)
                .into(imageView);
        if (!p.isConnected()) {
            imageView2.setVisibility(View.GONE);
        }
        // on positionne le email dans le TextView
        textView.setText(p.getEmail());
        Log.v("Androboum","bingo"+p.getEmail()) ;

        textScore.setText(String.valueOf(p.getScore()));
        Button bouton = layout.findViewById(R.id.buttonOther);

        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroBoumApp.getBomber().setBomb(p, new Bomber.BomberInterface()
                {
                    @Override
                    public void userBombed() { }

                    @Override
                    public void userBomber() {
                        // on lance l'activité de contrôle de la bombe
                        Intent intent = new Intent(context, BombActivity.class);
                        context.startActivity(intent);
                    }
                });
            }
        });

    }
}

package com.example.sakharu.androboum;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AndroBoumApp extends android.app.Application {

    public static Bomber getBomber() {
        return bomber;
    }

    private static Bomber bomber;

    static public void setCallback(Bomber.BomberInterface callback) {
        bomber.setCallback(callback);
    }

    static public void setContext(Context context) {
        bomber.setContext(context);
    }


    @Override
    public void onCreate() { super.onCreate(); }

    static public void buildBomber(Context c)   {
        bomber = new Bomber(c);
    }

    // methode pour se déconnecter lorsque l'on quitte l'application
    static public void setIsConnected(boolean connectStatus)
    {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser fuser = auth.getCurrentUser();
        final FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mreference;
        if (fuser != null)
        {
            mreference = mDatabase.getReference().child("Users").child(fuser.getUid());
            mreference.child("connected").setValue(connectStatus);

        }
    }


}

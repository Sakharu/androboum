package com.example.sakharu.androboum;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;

public class UserActivity extends AppCompatActivity implements View.OnClickListener {

    //  on choisit une valeur arbitraire pour représenter la connexion
    private static final int RC_SIGN_IN = 123;
    // défini un numéro unique pour repérer plus tard ce code
// dans la méthode onActivityResult(…)
    private static final int SELECT_PICTURE = 124;
    FirebaseAuth auth;
    private Profil user;
    private FusedLocationProviderClient mFusedLocationClient;
    private final int MY_PERMISSIONS_REQUEST=987;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        user = new Profil();

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        // on demande une instance du mécanisme d'authentification
        auth = FirebaseAuth.getInstance();
        // la méthode ci-dessous renvoi l'utilisateur connecté ou null si personne
        if (auth.getCurrentUser() != null)
        {
            // déjà connecté
            Log.v("AndroBoum","je suis déjà connecté sous l'email :" +auth.getCurrentUser().getEmail());
            ((TextView) findViewById(R.id.emailuser)).setText(auth.getCurrentUser().getEmail());
            downloadImage();
            setUser();
            updateProfil(user);
            MapsActivity.monmail=auth.getCurrentUser().getEmail();
        }
        else
        {
            // on lance l'activité qui gère l'écran de connexion en la paramétrant avec les providers googlet et facebook.
            startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                    .setAvailableProviders(Arrays.asList(new AuthUI.IdpConfig.GoogleBuilder().build(),new AuthUI.IdpConfig.FacebookBuilder().build()))
                    .build(), 123);
        }
        ImageView imageView = findViewById(R.id.iprofilother); imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(); intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);

                Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                intent.setAction(Intent.ACTION_PICK);
                Intent chooserIntent = Intent.createChooser(intent, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[] { captureIntent });
                startActivityForResult(chooserIntent, SELECT_PICTURE);
                return true;
            }
        });

        findViewById(R.id.buttonList).setOnClickListener(this);

        startService(new Intent(this, NearbyService.class));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_settings:
                // choix de l'action "Paramètres", on ne fait rien
                // pour l'instant
                return true;
            case R.id.action_map:
                Intent i = new Intent(this, MapsActivity.class);
                startActivity(i);
                return true;
            case R.id.action_logout:
                // choix de l'action logout
                // on déconnecte l'utilisateur
                //AndroBoumApp.setIsConnected(false);
                user.setConnected(false);
                FirebaseAuth auth = FirebaseAuth.getInstance();
                if (auth != null)
                {
                    FirebaseUser fuser = auth.getCurrentUser();
                    if (fuser != null)
                    {
                        final FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
                        DatabaseReference mreference = mDatabase.getReference().child("Users").child(fuser.getUid());
                        mreference.child("connected").setValue(false);
                    }
                }
                AuthUI.getInstance().signOut(this);
                finish();
                return true;
            default:
                /// aucune action reconnue
                return super.onOptionsItemSelected(item);

        }
    }


    // cette méthode est appelée quand l'appel StartActivityForResult est terminé
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // on vérifie que la réponse est bien liée au code de connexion choisi
        if (requestCode == RC_SIGN_IN)
        {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            // Authentification réussie
            if (resultCode == RESULT_OK)
            {
                assert response != null;
                Log.v("AndroBoum","je me suis connecté et mon email est :"+ response.getEmail());
                downloadImage();
                setUser();
                return;
            }
            else
            {
                // echec de l'authentification
                if (response == null)
                {
                    // L'utilisateur a pressé "back", on revient à l'écran
                    // principal en fermant l'activité
                    Log.v("AndroBoum","Back Button appuyé");
                    finish();
                    return;
                }
                // pas de réseau
                if (response.getError() !=null && response.getError().getErrorCode() == ErrorCodes.NO_NETWORK)
                {
                    Log.v("AndroBoum","Erreur réseau");
                    finish();
                    return;
                }

                // une erreur quelconque
                if (response.getError() !=null && response.getError().getErrorCode() == ErrorCodes.UNKNOWN_ERROR)
                {
                    Log.v("AndroBoum","Erreur inconnue");
                    finish();
                    return;
                }
            }
            Log.v("AndroBoum","Réponse inconnue");

        }
        if (requestCode == SELECT_PICTURE)
        {
            if (resultCode == RESULT_OK)
            {
                try
                {
                    ImageView imageView = findViewById(R.id.iprofilother);
                    boolean isCamera = (data.getData() == null);
                    final Bitmap selectedImage;
                    if (!isCamera)
                    {
                        final Uri imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        selectedImage = BitmapFactory.decodeStream(imageStream);
                        // on redimensionne le bitmap pour ne pas qu'il soit trop grand
                        Bitmap finalbitmap = Bitmap.createScaledBitmap(selectedImage, 500, (selectedImage.getHeight() * 500) / selectedImage.getWidth(), false);
                        imageView.setImageBitmap(finalbitmap);
                    }
                    else
                    {

                        if (data.getExtras()!=null) {
                            selectedImage = (Bitmap) data.getExtras().get("data");
                            // on redimensionne le bitmap pour ne pas qu'il soit trop grand
                            assert selectedImage != null;
                            Bitmap finalbitmap = Bitmap.createScaledBitmap(selectedImage, 500, (selectedImage.getHeight() * 500) / selectedImage.getWidth(), false);
                            imageView.setImageBitmap(finalbitmap);
                        }
                    }

                }
                catch (Exception e)
                {
                    Log.v("AndroBoum",e.getMessage());
                }
            }
            uploadImage();
        }
    }

    private StorageReference getCloudStorageReference()
    {     // on va chercher l'email de l'utilisateur connecté
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth == null) return null;
        String email="non défini";
        if (auth.getCurrentUser()!=null)
        email = auth.getCurrentUser().getEmail();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        // on crée l'objet dans le sous-dossier de nom l'email
        return storageRef.child(email + "/photo.jpg");
    }
    private void downloadImage() {
        StorageReference photoRef = getCloudStorageReference();
        if (photoRef == null)
            return;
        ImageView imageView = findViewById(R.id.iprofilother);
        // Load the image using Glide
        GlideApp.with(this /* context */)
                .load(photoRef)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.ic_person_black_24dp)
                .into(imageView);

    }
    private void uploadImage() {
        StorageReference photoRef = getCloudStorageReference();
        if (photoRef == null) return;
        // on va chercher les données binaires de l'image de profil
        ImageView imageView = findViewById(R.id.iprofilother);
        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        // on lance l'upload
        UploadTask uploadTask = photoRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // si on est là, échec de l'upload
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
        {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
            {
                // ok, l'image est uploadée
                // on fait pop un toast d'information
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.imageuploaded), Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    private void setUser()
    {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser fuser = auth.getCurrentUser();
        if (fuser != null)
        {
            user.setUid(fuser.getUid());
            user.setEmail(fuser.getEmail());
            user.setConnected(true);
            TextView emailuser= findViewById(R.id.emailuser);
            emailuser.setText(fuser.getEmail());
        }
        AndroBoumApp.buildBomber(this);
        getLocation();


    }

    private void updateProfil(Profil user)
    {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = mDatabase.child("Users").child(user.getUid());
        ref.child("connected").setValue(true);
        ref.child("email").setValue(user.getEmail());
        ref.child("uid").setValue(user.getUid());
        ref.child("latitude").setValue(user.getLatitude());
        ref.child("longitude").setValue(user.getLongitude());
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, UserListActivity.class);
        startActivity(i);
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // on demande les permissions
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST);
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // location contient la position, sauf si il est null.
                        if (location != null)
                        {
                            Log.v("Androboum","Coordonnées GPS: Latitude="+ location.getLatitude()+" Longitude="+location.getLongitude());
                            user.setLatitude(location.getLatitude());
                            user.setLongitude(location.getLongitude());
                            updateProfil(user);
                        }
                    }
                });



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // on est autorisé, donc on rappelle getLocation()
                    getLocation();
                }
                else
                { Toast.makeText(getApplicationContext(),"Vous devez accepter les permissions pour que l'application fonctionne correctement !",Toast.LENGTH_SHORT).show(); }
            }
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        AndroBoumApp.setIsConnected(false);
    }
}


